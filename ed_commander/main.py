'''
Created on Mar 6, 2015

@author: Doug Kasten
'''

# Import PySide classes
import sys
from PySide.QtCore import *
from PySide.QtGui import *
 


def main():
    ''' Create the main window GUI for Dangerous Trade '''
    # Create a Qt application
    app = QApplication(sys.argv)
    
    # Create 
    label = QLabel("Hello World")
    label.show()
    
    # Enter Qt application main loop
    app.exec_()
    sys.exit()
 
 
 
if __name__ == '__main__':
    main()